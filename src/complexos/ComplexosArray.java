
package complexos;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author amartins
 */
public class ComplexosArray {
    private ArrayList<Complexos> vec;
    
    public ComplexosArray(){
        vec = new ArrayList<Complexos>();
    }
    
    public ComplexosArray(Complexos c){
        vec = new ArrayList<Complexos>();
        vec.add(c);
    }
    
    public double average() {
        double sum = 0;
        int num = 0;
        for(Complexos c : vec) {
            sum += c.modulus();
            num++;
        }
        return sum/num;
    }
    
    public void add(Complexos c) {
        vec.add(c);
        Collections.sort(vec);      // altamente ineficiente
    }
    
    public Complexos getElementI(int i) {
        if(i < vec.size())
            return vec.remove(i);
        else
            return null;
    }
    
    public int getSize() {
        return vec.size();
    }
    
    @Override
    public String toString() {
        String st = "[";
        for(Complexos c : vec) {
            st += c.toString();
        }
        st += "]";
        return st;
    }
}
