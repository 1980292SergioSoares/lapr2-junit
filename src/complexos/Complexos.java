
package complexos;

import java.util.Comparator;

/**
 *
 * @author Angelo Martins
 */

public class Complexos implements Comparable<Complexos> {
    double a, b;
    private double PrecComp = 0.005;

    public Complexos(double a, double b) {
        this.a=a;
        this.b=b;
    }
    public Complexos() {
        this.a=0;
        this.b=0;
    }
    
    public Complexos(String z) {
        String aux[] = new String[2];
        if( z.contains("+")) {
            aux = z.split("+");
        } else {
            if( z.contains("-"))
                aux = z.split("-");
        }
        a = Double.parseDouble(aux[0]);
        b = Double.parseDouble(aux[1]);
    }

    public Complexos add(Complexos z) {
       Complexos c = new Complexos();
       c.a = z.a + this.a;
       c.b = z.b + this.b;
       return c;
    }
    
    public Complexos subtract(Complexos z) {
       Complexos c = new Complexos();
       c.a = this.a - z.a;
       c.b = this.b - z.b;
       return c;
    }

    public Complexos multiply(Complexos z) {
       Complexos c = new Complexos();
       c.a = z.a * this.a - z.b * this.b;
       c.b = z.a * this.b + z.b * this.a;
       return c;
    }
    
    public Complexos divide(Complexos z) {
       Complexos c = new Complexos();
       c.a = (z.a * this.a + z.b * this.b)/(z.a*z.a +z.b*z.b);
       c.b = (z.a * this.b - this.a * z.b)/(z.a*z.a +z.b*z.b);
       return c;
    }
    
    public double modulus() {
        return Math.sqrt(this.a*this.a + this.b*this.b);
    }
    
    public double angle() {
        double res;
        if(this.a<0){
            res = Math.atan(Math.abs(this.a)/this.b);
            if(b>0)
                 res = Math.PI-res;
            else
                res = -Math.PI-res;
        }
        else
            res = Math.atan(this.a/this.b);
       return res;
    }
    
    public float angleDegrees() {
        return (float) (angle()/Math.PI/2*360);
    }
    
    public Complexos conjugate() {
       Complexos c = new Complexos();
       c.a = this.a;
       c.b = -this.b;
       return c;
    }
    
    public void shiftDirectDegrees(float rot) {
        double m = modulus();
        double ang = (angleDegrees()+rot)/360*2*Math.PI;
        
        this.a = m * Math.sin(ang);
        this.b = m * Math.cos(ang);
    }
    
    @Override
    public int compareTo(Complexos c) {
        
        double res = this.modulus() - c.modulus();
        
        if(res > 0) {
            return 1;
        }
        else if(res<0) {
            return -1;  
        }
        else
            return 0;
    }
    
    /**
     * 
     */
    public static Comparator<Complexos> ComplexosImagComparator
            = new Comparator<Complexos>() {
                
       @Override
       public int compare(Complexos c1, Complexos c2) {
           if(c1.b>=c2.b)
               return 1;
           else
               return -1;
       }
                
    };
    
    @Override
    public String toString() {
        return "(" + a + " + i" + b + ")";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object other){
        boolean result = false;
        if (other instanceof Complexos) {
            Complexos that = (Complexos) other;
            if(Double.isNaN(this.a) && Double.isNaN(that.a) ||
                    Double.isNaN(this.b) && Double.isNaN(that.b)) {
                result = true;
            }
            else
                result = (Math.abs(this.a - that.a)<this.PrecComp
                    && Math.abs(this.b - that.b)<this.PrecComp);
        }
        return result;
    }
} 
