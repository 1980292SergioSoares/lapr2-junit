/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexosTest {
    
    public ComplexosTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAdd2() {
        System.out.println("add 2 numeros");
        Complexos z1 = new Complexos(1,-1);
        Complexos z2 = new Complexos(2,1);
        Complexos expResult = new Complexos(3,0);
        Complexos result = z1.add(z2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAdd3() {
        System.out.println("add, mas 3 numeros");
        Complexos z1 = new Complexos(1,1);
        Complexos z2 = new Complexos(1,1);
        Complexos z3 = new Complexos(1,1);
        Complexos expResult = new Complexos(3,3);
        Complexos result = z3.add(z1.add(z2));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of multiply method, of class Complexos.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        Complexos z = new Complexos(1,-1);
        Complexos instance = new Complexos(1,1);
        Complexos expResult = new Complexos(2,0);
        Complexos result = instance.multiply(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Complexos.
     */
    @Test
    public void testToString() {
        double a = 1, b = 2;
        System.out.println("toString");
        Complexos instance = new Complexos(a,b);
        String expResult = "(" + a + " + i" +b + ")";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class Complexos.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Complexos z = new Complexos(1,0);
        Complexos instance = new Complexos(1,1);
        Complexos expResult = new Complexos(2,1);
        Complexos result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of getModulo method, of class Complexos.
     */
    @Test
    public void testModulus() {
        System.out.println("getModulo");
        Complexos instance = new Complexos(3,4);
        double expResult = 5;
        double result = instance.modulus();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of compareTo method, of class Complexos.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Complexos c = new Complexos(3,4);
        Complexos instance = new Complexos(4,3);
        int expResult = 0;
        int result = instance.compareTo(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Complexos.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Complexos(-2, 5);
        Complexos instance = new Complexos(-2, 5);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals2() {
        System.out.println("equals");
        Object other = new Complexos(-2, 5);
        Complexos instance = new Complexos(2, 5);
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

     /**
     * Test of equals method, of class Complexos.
     */
    @Test
    public void testEqualsNaN() {
        System.out.println("equals with NaN");
        Object other = new Complexos(Double.NaN, Double.NaN);
        Complexos instance = new Complexos(Double.NaN, Double.NaN);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
    /**
     * Test of subtract method, of class Complexos.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        Complexos z = new Complexos(3, 1);
        Complexos instance = new Complexos(0,1);
        Complexos expResult = new Complexos(-3, 0);
        Complexos result = instance.subtract(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of divide method, of class Complexos.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        Complexos z = new Complexos (3,0);
        Complexos instance = new Complexos(0,1);
        Complexos expResult = new Complexos(0,0.333);
        Complexos result = instance.divide(z);
        assertEquals(expResult, result);
    }

        /**
     * Test of divide method, of class Complexos.
     */
    @Test
    public void testDivideZero() {
        System.out.println("divide");
        Complexos z = new Complexos (0,0);
        Complexos instance = new Complexos(3,1);
        Complexos expResult = new Complexos(Double.NaN, Double.NaN);
        Complexos result = instance.divide(z);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle() {
        System.out.println("angle");
        Complexos instance = new Complexos(2,2);
        double expResult = Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle2Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(-2,2);
        double expResult = 3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }

     /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle3Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(-2,-2);
        double expResult = -3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle4Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(2,-2);
        double expResult = -Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class Complexos.
     */
    @Test
    public void testAngleDegrees() {
        System.out.println("angleDegrees");
        Complexos instance = new Complexos(-2,2);
        float expResult = 135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class Complexos.
     */
    @Test
    public void testAngleDegrees3Q() {
        System.out.println("angleDegrees");
        Complexos instance = new Complexos(-2,-2);
        float expResult = -135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }

    /**
     * Test of conjugate method, of class Complexos.
     */
    @Test
    public void testConjugate() {
        System.out.println("conjugate");
        Complexos instance = new Complexos(2, 3);
        Complexos expResult = new Complexos (2, -3);
        Complexos result = instance.conjugate();
        assertEquals(expResult, result);
    }

    /**
     * Test of shiftDirectDegrees method, of class Complexos.
     */
    @Test
    public void testShiftDirectDegrees() {
        System.out.println("shiftDirectDegrees");
        float rot = 90F;
        Complexos instance = new Complexos(1, 1);
        instance.shiftDirectDegrees(rot);
        Complexos expResult = new Complexos (1, -1);
        assertEquals(expResult, instance);
    }
}
